package com.opkloud.kloudprints.gradle.extensions

import org.apache.commons.lang3.StringUtils
import org.gradle.api.GradleException

class KloudPrintExtensionDone {

    String deploymentBucket
    String awsRegion
    boolean deleteStacks
    boolean singleton

    List<String> order = new ArrayList<>()
    List<String> puppetParams = new ArrayList<>()
    List<String> labels = new ArrayList<>()

    Map<String, Map<String, Object>> dependencies = new LinkedHashMap<>()

    Map<String, Map<String, Object>> params = new HashMap<>()
    Map<String, Object> flattenedParams = new HashMap<>()
    Map<String, Map<String, Object>> exposedParams = new HashMap<>()
    Map<String, Map<String, Object>> requiredParams = new HashMap<>()

    void awsRegion(String awsRegion){
        this.awsRegion = awsRegion
    }

    void deploymentBucket(String deploymentBucket){
        this.deploymentBucket = deploymentBucket
    }

    void deleteStacks(boolean deleteStacks){
        this.deleteStacks = deleteStacks
    }

    void singleton(boolean singleton){
        this.singleton = singleton
    }

    void order(String... kloudprint) {
        order.addAll(kloudprint)
    }

    void puppetParams(String... params){
        puppetParams.addAll(params)
    }

    void param(String paramNameOrShorthand, String paramDefaultValue = null, boolean required = false, boolean exposed = true) {
        if (!paramNameOrShorthand.contains(":")) {
            Map<String, Object> paramConfig = [
                    name    : paramNameOrShorthand,
                    value   : paramDefaultValue,
                    required: required,
                    exposed : exposed
            ]

            this.param(paramConfig)
        } else {
            this.processShorthandParam(paramNameOrShorthand)
        }

    }

    void processShorthandParam(String paramString) {
        String[] paramProps = paramString.split(":")
        Map<String, Object> paramConfig = new HashMap<>()

        if (paramProps.size() == 2) {
            paramConfig = [name: paramProps[0], value: paramProps[1]]
        } else if (paramProps.size() == 3) {
            if (paramProps[2].equalsIgnoreCase("required")) {
                paramConfig = [name: paramProps[0], value: paramProps[1], required: true]
            } else {
                paramConfig = [name: paramProps[0], value: paramProps[1], exposed: true]
            }
        } else if (paramProps.size() == 4) {
            paramConfig = [name: paramProps[0], value: paramProps[1], required: true, exposed: false]
        }
        this.param(paramConfig)
    }


    void param(Map<String, Object> paramConfig) {

        validateParams(paramConfig)

        this.params.put(paramConfig.name, paramConfig)
        this.flattenedParams.put(paramConfig.name, paramConfig.value)

        if (paramConfig.exposed == true) {
            this.exposedParams.put(paramConfig.name, paramConfig)
        }

        if (paramConfig.required == true) {
            this.requiredParams.put(paramConfig.name, paramConfig)
        }

    }

    private void validateParams(Map<String, Object> paramConfig) {
        if (paramConfig == null) {
            // Throw Exception
        }

        // Ensure there is a paramConfig.name
        if (StringUtils.isBlank(paramConfig.name)) {
            // Throw Exception
        }

        if (paramConfig.name != null && paramConfig.value == null) {
            paramConfig.required = true
            paramConfig.exposed = true
        }

        if (paramConfig.name != null && paramConfig.value != null && paramConfig.required == null && paramConfig.exposed == null) {
            paramConfig.required = false
            paramConfig.exposed = true
        }

        if (paramConfig.name != null && paramConfig.value != null && paramConfig.required != null && paramConfig.exposed == null) {
            paramConfig.exposed = true
        }

        if (paramConfig.name != null && paramConfig.value != null && paramConfig.required == null && paramConfig.exposed != null) {
            paramConfig.required = false
        }

        if ((paramConfig.exposed == false && paramConfig.required == false) || (paramConfig.required == true || paramConfig.exposed == false)) {
            //throw error
        }
    }

    void dependency(String label, String artifactID, Map<String, Object> artifactConfig) {

        if(labels.contains(label)){
            throw new GradleException("Dependency labels must be unique. " + label + " exists already.")
        }

        labels.add(label)
       this.dependencies.put(label + "~" + artifactID, artifactConfig)
    }


}

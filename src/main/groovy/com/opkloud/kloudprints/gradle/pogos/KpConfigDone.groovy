package com.opkloud.kloudprints.gradle.pogos

/**
 * Created by marco on 11/29/17.
 */
class KpConfigDone {

    String deploymentBucket
    String awsRegion

    boolean deleteStacks = false
    boolean singleton = false

    List<String> puppetParams = new ArrayList<>()

    Map<String,Object> flattenedParams = new HashMap<>()

    Map<String, Map<String, Object>> dependencies = new HashMap<>()
    Map<String, Map<String, Object>> params = new HashMap<>()
    Map<String, Map<String, Object>> exposedParams = new HashMap<>()
    Map<String, Map<String, Object>> requiredParams = new HashMap<>()

}

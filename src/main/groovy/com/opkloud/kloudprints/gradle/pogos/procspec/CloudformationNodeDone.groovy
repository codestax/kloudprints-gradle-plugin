package com.opkloud.kloudprints.gradle.pogos.procspec

/**
 * Created by marco on 12/13/17.
 */
class CloudformationNodeDone {
    String type = "action"
    String id = "cloudformation"
    String action = "cloud_formation_minion:createStack"
    String aws_region
    String aws_stack_name
    String cf_script_filename
    Map<String,Object> cf_parameters = new HashMap<>()
}

package com.opkloud.kloudprints.gradle.plugins

import com.opkloud.kloudprints.gradle.extensions.KloudPrintExtensionDone
import com.opkloud.kloudprints.gradle.tasks.GenerateConfigurationTaskDone
import com.opkloud.kloudprints.gradle.tasks.KloudDeployTaskDone

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.artifacts.Configuration
import org.gradle.api.plugins.JavaPluginConvention
import org.gradle.api.tasks.SourceSet
import org.gradle.jvm.tasks.Jar

class KloudPrintsPluginDone implements Plugin<Project> {

    private static final String TASK_GROUP_NAME = "KloudPrints"

    private static final String DEPENDENCY_CONFIGURATION_NAME = "kloudprint"
    private static final String KLOUD_EXTRACT_TASK_NAME = "kpExtract"
    private static final String KLOUD_DEPLOY_TASK_NAME = "kpDeploy"
    private static final String KLOUD_PACKAGE_TASK_NAME = "kpPackage"
    private static final String GENERATE_CONFIG = "generateKpConfig"

    private static final String DEPENDENCY_CONFIGURATION_DESCRIPTION = "The KloudPrints to retrieve for this project."
    private static final String KLOUD_EXTRACT_TASK_DESCRIPTION = "Extracts content of specified KloudPrints to directory."
    private static final String KLOUD_DEPLOY_TASK_DESCRIPTION = "Deploys infrastructure defined by the process-spec and KloudPrint defined."
    private static final String KLOUD_PACKAGE_TASK_DESCRIPTION = "Packages a KloudPrint project into a jar file"
    private static final String GENERATE_CONFIG_TASK_DESCRIPTION = "Generates kloudprints-props for the kloudprints"

    private static final String DEFAULT_INFRA_DIR = "src/main/infrastructure"
    private static final String DEFAULT_RESOURCES_DIR = "src/main/resources"

    void apply(Project project) {

        final Configuration config = project.getConfigurations().create(DEPENDENCY_CONFIGURATION_NAME)
                .setVisible(true)
                .setTransitive(true)
                .setDescription(DEPENDENCY_CONFIGURATION_DESCRIPTION)

        project.getConfigurations().getByName("compile").extendsFrom(config)

        KloudPrintExtensionDone kloudPrintExtension = project.extensions.create('kloudprints', KloudPrintExtensionDone)

        configureKloudExtractorTask(project, config)
        configureKloudDeployTask(project, kloudPrintExtension, config)
        configureKloudPackageTask(project, configureSourceSets(project))
        configureGenerateKPConfig(project, kloudPrintExtension)
    }

    void configureKloudExtractorTask(Project project, Configuration config) {
        Task kmTask = project.getTasks().create(KLOUD_EXTRACT_TASK_NAME, KloudPrintExtractorTaskDone.class, { task ->
            task.setKloudPrints(config)
        })
        kmTask.setGroup(TASK_GROUP_NAME)
        kmTask.setDescription(KLOUD_EXTRACT_TASK_DESCRIPTION)
    }

    void configureKloudDeployTask(Project project, KloudPrintExtensionDone kloudPrintExtension, Configuration config) {
        Task kmTask = project.getTasks().create(KLOUD_DEPLOY_TASK_NAME, KloudDeployTaskDone.class, { task ->
            task.setConfigurations(kloudPrintExtension)
            task.setKloudPrints(config)
        })

        kmTask.dependsOn("build", KLOUD_EXTRACT_TASK_NAME)
        kmTask.setGroup(TASK_GROUP_NAME)
        kmTask.setDescription(KLOUD_DEPLOY_TASK_DESCRIPTION)
    }

    void configureKloudPackageTask(Project project, SourceSet main) {
        Task kmTask = project.getTasks().create(KLOUD_PACKAGE_TASK_NAME, Jar.class, { task ->
            task.includeEmptyDirs = true
            task.from main.resources
        })
        kmTask.dependsOn(GENERATE_CONFIG)
        kmTask.setGroup(TASK_GROUP_NAME)
        kmTask.setDescription(KLOUD_PACKAGE_TASK_DESCRIPTION)
    }

    void configureGenerateKPConfig(Project project, KloudPrintExtensionDone kloudPrintExtension) {
        Task kmTask = project.getTasks().create(GENERATE_CONFIG, GenerateConfigurationTaskDone.class, { task ->
            task.setConfigurations(kloudPrintExtension)
        })
        kmTask.setGroup(TASK_GROUP_NAME)
        kmTask.setDescription(GENERATE_CONFIG_TASK_DESCRIPTION)
    }

    SourceSet configureSourceSets(Project project){
        JavaPluginConvention javaPluginConvention = project.getConvention().getPlugin(JavaPluginConvention.class)
        SourceSet main = javaPluginConvention.getSourceSets().getByName(SourceSet.MAIN_SOURCE_SET_NAME)
        main.resources {dir ->
            dir.srcDirs(DEFAULT_INFRA_DIR, DEFAULT_RESOURCES_DIR)
        }
        return main
    }

}

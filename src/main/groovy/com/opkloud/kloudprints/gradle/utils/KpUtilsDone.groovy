package com.opkloud.kloudprints.gradle.utils

import com.opkloud.kloudprints.gradle.extensions.KloudPrintExtensionDone
import com.opkloud.kloudprints.gradle.pogos.KpConfigDone

/**
 * Created by marco on 11/29/17.
 */
class KpUtilsDone {

    static KpConfigDone convertToConfig(KloudPrintExtensionDone extension){
        KpConfigDone config = new KpConfigDone()

        config.setParams(extension.params)
        config.setRequiredParams(extension.requiredParams)
        config.setDependencies(extension.dependencies)
        config.setExposedParams(extension.exposedParams)
        config.setDeploymentBucket(extension.deploymentBucket)
        config.setPuppetParams(extension.puppetParams)
        config.setDeleteStacks(extension.deleteStacks)
        config.setSingleton(extension.singleton)
        config.setFlattenedParams(extension.flattenedParams)
        config.setAwsRegion(extension.awsRegion)

        return config
    }


}

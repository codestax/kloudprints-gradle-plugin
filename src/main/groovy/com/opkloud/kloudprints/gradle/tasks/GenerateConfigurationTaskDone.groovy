package com.opkloud.kloudprints.gradle.tasks

import com.opkloud.kloudprints.gradle.extensions.KloudPrintExtensionDone
import com.opkloud.kloudprints.gradle.utils.KpUtilsDone
import groovy.json.JsonOutput
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

class GenerateConfigurationTaskDone extends DefaultTask {


    private String DEFAULT_PROPERTIES_NAME = '/kloud-properties.json'

    private KloudPrintExtensionDone extension

    void setConfigurations(KloudPrintExtensionDone extension){
        this.extension = extension
    }

    @TaskAction
    void generate() {
        File kpPropsFile = new File("src/main/resources" + DEFAULT_PROPERTIES_NAME)
        String json = JsonOutput.prettyPrint(JsonOutput.toJson(KpUtilsDone.convertToConfig(extension)))

        if(kpPropsFile.exists()){
            kpPropsFile.delete()
        }

        kpPropsFile.write json

        println(kpPropsFile.absolutePath)

        File distribution = new File("src/main/infrastructure/distribution")
        if(distribution.exists() && distribution.isDirectory()){
            if(distribution.list().length>0){
                project.ant.zip(destfile: "src/main/resources/infrastructure.zip",
                        basedir: distribution.absolutePath)
            }
        }
    }

}

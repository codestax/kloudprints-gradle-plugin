package com.opkloud.kloudprints.gradle.tasks

import com.amazonaws.auth.AWSCredentials
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain
import com.opkloud.kloudminions.process.Process
import com.opkloud.kloudminions.process.ProcessBuilder
import com.opkloud.kloudprints.gradle.extensions.KloudPrintExtensionDone
import com.opkloud.kloudprints.gradle.pogos.KpConfigDone
import com.opkloud.kloudprints.gradle.utils.KpUtilsDone
import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import org.apache.commons.lang3.RandomStringUtils
import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.TaskAction


class KloudDeployTaskDone extends DefaultTask {

    private static List<String> ignoredParams = ["awsRegion","stackName"]

    private FileCollection kloudprints

    private String DEFAULT_BUILD_PATH = System.getProperty("user.dir") + File.separator + "build" + File.separator + "tmp" + File.separator
    private String DEFAULT_PROPERTIES_NAME = /*File.separator + */"kloud-properties.json"
    private String awsRegion

    private KloudPrintExtensionDone extension
    private KpConfigDone kpConfig

    private Map<String,String> existingStacks = new HashMap<>()

    private Map procSpecMap = [
            type:"sequence",
            nodes:[] as LinkedList
    ]

    void setConfigurations(KloudPrintExtensionDone extension) {
        this.extension = extension
    }

    @InputFiles
    FileCollection getKloudPrints() {
        return kloudprints
    }

    void setKloudPrints(FileCollection kloudprints) {
        this.kloudprints = kloudprints
    }


    @TaskAction
    def deploy() {

        println "\n\n\n\n\n\n\n Starting deploy() in KloudDeployTask \n\n\n\n\n\n\n\n\n"
        this.kpConfig = KpUtilsDone.convertToConfig(extension)
        this.awsRegion = this.kpConfig.awsRegion

        if(!this.kpConfig.dependencies.isEmpty()){
            executeKp(this.kpConfig)
        }

        println "\n\n\n\n\n\n\n Starting deploy() in KloudDeployTask \n\n\n\n\n\n\n\n\n"

    }

    void executeKp(KpConfigDone parentLevelConfig) {

        generateProcSpec(parentLevelConfig)

        //add propfile to front of procspec
        this.addS3UploadNode(procSpecMap, awsRegion, DEFAULT_BUILD_PATH + "kpFacterProps.json", null, true)

        if (this.kpConfig.deleteStacks == true) {
            List<Object> reversibleNodes = new LinkedList<>()

            for(Object node: (List)procSpecMap.nodes){
                reversibleNodes.add(node.clone())
            }

            Collections.reverse(reversibleNodes)

            List<Object> nodesToRemove = new ArrayList<>()
            reversibleNodes.each { node ->
                if (node.action == "cloud_formation_minion:createStack") {
                    node.action = "cloud_formation_minion:deleteStack"
                }
                else {
                    nodesToRemove.add(node)
                }
            }
            reversibleNodes.removeAll(nodesToRemove)

            procSpecMap.nodes = reversibleNodes + procSpecMap.nodes
            executeProcSpec(procSpecMap)
        } else {
            executeProcSpec(procSpecMap)
        }
    }

    void generateProcSpec(KpConfigDone parentLevelConfig){
        for(String dependencyName: parentLevelConfig.dependencies.keySet()){

            println "\n\n\n\n\n\n\n In generateProcSpec() in KloudDeployTask"
            println "dependencyName: " + dependencyName

            // An exemplar dependencyName ==> tomcat1~com.opkloud.kloudprints:tomcat-kp:1.1.0
            String[] dependencyParts = dependencyName.split("~")

            String dependencyLabel = dependencyParts[0]

            // Example dependencyNameDir ==> com.opkloud.kloudprints-tomcat-kp-1.1.0
            String dependencyNameDir = dependencyParts[1].replaceAll(":","-")


            println "dependencyParts.size(): " + dependencyParts.size()
            println "dependencyParts[0]: " + dependencyParts[0]
            println "dependencyParts[1]: " + dependencyParts[1]


            File dependencyNameDirAbsolutePathFile = new File(DEFAULT_BUILD_PATH + dependencyNameDir + File.separator + DEFAULT_PROPERTIES_NAME)


            println "dependencyNameDirAbsolutePathFile: " + dependencyNameDirAbsolutePathFile
            println "\n\n\n\n\n\n\n\n"


            KpConfigDone currentLevelConfig = new KpConfigDone(new JsonSlurper().parse(dependencyNameDirAbsolutePathFile))

            currentLevelConfig.flattenedParams << parentLevelConfig.dependencies.get(dependencyName)
//            this.awsRegion = currentLevelConfig.flattenedParams.get("awsRegion")

            if(!currentLevelConfig.dependencies.isEmpty()) {
                this.generateProcSpec(currentLevelConfig)
            }

            validateParams(parentLevelConfig, currentLevelConfig, dependencyName)

            String infrastructureDeploymentPath = DEFAULT_BUILD_PATH + dependencyNameDir + File.separator + "infrastructure.zip"

            File infrastructureDir = new File(infrastructureDeploymentPath)

            String s3Key = dependencyLabel+"-"+getHex()

            if(currentLevelConfig.flattenedParams.containsKey("deployment")){
                File deploymentPath = new File(currentLevelConfig.flattenedParams.get("deployment"))
                File deploymentPackage = new File(DEFAULT_BUILD_PATH + dependencyNameDir + File.separator + s3Key + File.separator + "deployment.zip")

                if(deploymentPackage.exists()){
                    deploymentPackage.delete()
                }

                if(deploymentPath.isDirectory()){
                    project.ant.zip(destfile: deploymentPackage.absolutePath,
                            basedir: deploymentPath.absolutePath)
                }
                else {
                    currentLevelConfig.puppetParams.add("deploymentfilename")
                    currentLevelConfig.flattenedParams.put("deploymentfilename", deploymentPath.name)

                    project.ant.zip(destfile: deploymentPackage.absolutePath) {
                        fileset(file: deploymentPath.absolutePath)
                    }
                }


//                this.addS3UploadNode(procSpecMap,currentLevelConfig.flattenedParams.get("awsRegion"),deploymentPackage.absolutePath, s3Key)
                this.addS3UploadNode(procSpecMap,awsRegion,deploymentPackage.absolutePath, s3Key)
                currentLevelConfig.flattenedParams.remove("deployment")
            }

            if(infrastructureDir.exists() && infrastructureDir.isFile()){

                currentLevelConfig.flattenedParams.put("DeploymentBucket", this.kpConfig.deploymentBucket)
                currentLevelConfig.flattenedParams.put("BucketKey", s3Key)

//                this.addS3UploadNode(procSpecMap,currentLevelConfig.flattenedParams.get("awsRegion"),puppetDeploymentPath, s3Key)
                this.addS3UploadNode(procSpecMap,awsRegion,infrastructureDeploymentPath, s3Key)
            }

            generateFacterPropFile(currentLevelConfig.puppetParams, currentLevelConfig.flattenedParams)

            if(currentLevelConfig.isSingleton()){
//                if((existingStacks.containsKey(dependencyNameDir) && existingStacks.get(dependencyNameDir) != currentLevelConfig.flattenedParams.get("awsRegion")) ||
                if((existingStacks.containsKey(dependencyNameDir) && existingStacks.get(dependencyNameDir) != awsRegion ||
                        (!existingStacks.containsKey(dependencyNameDir)))){
//                    existingStacks.put(dependencyNameDir,currentLevelConfig.flattenedParams.get("awsRegion"))
                    existingStacks.put(dependencyNameDir,awsRegion)
                    this.addCloudformationNode(procSpecMap,currentLevelConfig.flattenedParams,dependencyNameDir)
                }
            }
            else {
//                existingStacks.put(dependencyNameDir,currentLevelConfig.flattenedParams.get("awsRegion"))
                existingStacks.put(dependencyNameDir,awsRegion)
                this.addCloudformationNode(procSpecMap,currentLevelConfig.flattenedParams,dependencyNameDir)
            }
        }
    }


    private void validateParams(KpConfigDone parentLevelConfig, KpConfigDone currentLevelConfig, String dependencyName){

        //check required params
        for(String requiredName: currentLevelConfig.requiredParams.keySet()){
            if(!parentLevelConfig.dependencies.get(dependencyName).containsKey(requiredName)){
                throw new GradleException("Required parameter was not found: " + requiredName)
            }
        }
        //check if params provided are exposed/valid params
        for(String exposedParam: parentLevelConfig.dependencies.get(dependencyName).keySet()){
            if(!currentLevelConfig.exposedParams.containsKey(exposedParam)){
                throw new GradleException("Parameter isn't exposed/valid: " + exposedParam)
            }
        }
    }

    private Map<String, Object> filterConfig(Map<String, Object> config){
        ignoredParams.each {param ->
            config.remove(param)
        }

        return config
    }

    private executeProcSpec(Map<String, Object> procSpecMap){
        ProcessBuilder processBuilder = new ProcessBuilder()

        processBuilder.setProcessSpec(procSpecMap)
        processBuilder.setGlobalContextMap(new HashMap<String, Object>())


        AWSCredentials defaultCredentials = new DefaultAWSCredentialsProviderChain().credentials
        processBuilder.setAWSCredentials(defaultCredentials)

        try{
            Process process = processBuilder.build()
            process.execute()
        }
        catch (Exception e){
            throw new GradleException("There was an exception executing a kloudprint: ",e)
        }

    }


    private void addCloudformationNode(Map procSpecMap, Map<String, Object> flattenedConfig, String dependencyName ){

        procSpecMap.nodes.add(

                [
                        type : "action",
                        id : "cloudformation",
                        action :  "cloud_formation_minion:createStack",
                        aws_region : awsRegion,
                        aws_stack_name :  flattenedConfig.get("stackName"),
                        DeploymentBucket : this.kpConfig.deploymentBucket,
                        cf_script_filename :  DEFAULT_BUILD_PATH + dependencyName + File.separator + "cloudformations" + File.separator + "default.cf",
                        cf_parameters : filterConfig(flattenedConfig)
                ]
        )
    }

    private void addS3UploadNode(Map procSpecMap, String awsRegion, String fileToUpload, String s3Dir = null, boolean addToFront = false){

        /* TODO: Refactor - both clauses appear to be identical */

        if(addToFront){
            procSpecMap.nodes.addFirst(

                    [
                            type : "action",
                            id : "s3Upload",
                            aws_region : awsRegion,
                            action : "s3_minion:uploadFiles",
                            files : [fileToUpload],
                            s3_dir : s3Dir,
                            bucket: this.kpConfig.deploymentBucket
                    ]
            )
        }
        else {
            procSpecMap.nodes.add(

                    [
                            type : "action",
                            id : "s3Upload",
                            aws_region : awsRegion,
                            action : "s3_minion:uploadFiles",
                            files : [fileToUpload],
                            s3_dir : s3Dir,
                            bucket: this.kpConfig.deploymentBucket
                    ]
            )
        }


    }

    private void generateFacterPropFile(List<String> puppetParamsList, Map<String, Object> flattenedParams){
        Map<String, Object> puppetParams = extractPuppetParams(puppetParamsList, flattenedParams)

        File propFile = new File(DEFAULT_BUILD_PATH + "kpFacterProps.json")
        String json = JsonOutput.prettyPrint(JsonOutput.toJson(puppetParams))

        //if exists read in and append
        if(propFile.exists()){
            Map<String, Object> contents =  new JsonSlurper().parse(new File(DEFAULT_BUILD_PATH + "kpFacterProps.json"))
            contents << puppetParams
            propFile.write JsonOutput.prettyPrint(JsonOutput.toJson(contents))
        }
        else {
            propFile.write json
        }

    }

    private Map<String, Object> extractPuppetParams(List<String> puppetParams, Map<String, Object> params){
        Map<String, Object> results = new HashMap<>()

        puppetParams.each {puppetParam ->
            if(params.get(puppetParam) == null){
                throw new GradleException("Puppet param isn't defined in the params: " + puppetParam)
            }
            else {
                results.put(puppetParam, params.get(puppetParam))
                params.remove(puppetParam)
            }
        }

        return results
    }

    /* TODO: Let's look at this as a way to create random S3 Key Names... maybe a UUID might work better. */
    private String getHex(){
        return RandomStringUtils.randomNumeric(4)
    }
}

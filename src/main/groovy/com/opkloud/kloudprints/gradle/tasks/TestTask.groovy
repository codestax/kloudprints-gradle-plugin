package com.opkloud.kloudprints.gradle.tasks

import com.opkloud.kloudprints.gradle.extensions.KloudPrintExtensionDone
import com.opkloud.kloudprints.gradle.pogos.KpConfigDone
import com.opkloud.kloudprints.gradle.utils.KpUtilsDone
import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.tasks.TaskAction

class TestTask extends DefaultTask {

    private KloudPrintExtensionDone extension
    private KpConfigDone kpConfig

    private String DEFAULT_BUILD_PATH = System.getProperty("user.dir")+ '/build/tmp/'
    private String DEFAULT_PROPERTIES_NAME = '/kloud-properties.json'

    private List<String> existingStacks = new ArrayList<>()

    private Map procSpecMap = [
            type:"sequence",
            nodes:[] as LinkedList
    ]

    @TaskAction
    def deploy() {

        this.kpConfig = KpUtilsDone.convertToConfig(extension)

        if(!this.kpConfig.dependencies.isEmpty()){
            executeKp(this.kpConfig)
        }
    }

    void executeKp(KpConfigDone parentLevelConfig) {

        generateProcSpec(parentLevelConfig)
    }

    void generateProcSpec(KpConfigDone parentLevelConfig){
        for(String dependencyName: parentLevelConfig.dependencies.keySet()){
            String dependencyNameDir = dependencyName.replaceAll(":","-")

            KpConfigDone currentLevelConfig = new KpConfigDone(new JsonSlurper().parse(new File(DEFAULT_BUILD_PATH + dependencyNameDir + File.separator + DEFAULT_PROPERTIES_NAME)))
            currentLevelConfig << parentLevelConfig.dependencies.get(dependencyName)
            this.awsRegion = currentLevelConfig.flattenedParams.get("awsRegion")

            if(!currentLevelConfig.dependencies.isEmpty()){
                this.generateProcSpec(currentLevelConfig)
            }

            validateParams(parentLevelConfig, currentLevelConfig, dependencyName)

            String puppetDeploymentPath = DEFAULT_BUILD_PATH + dependencyNameDir + File.separator + "puppet.zip"

            File puppetDir = new File(puppetDeploymentPath)

            if(puppetDir.exists() && puppetDir.isFile()){
                currentLevelConfig.flattenedParams.put("DeploymentBucket", this.kpConfig.deploymentBucket)
                String s3Dir = dependencyName[dependencyName.indexOf(":") + 1 .. dependencyName.lastIndexOf(":")-1]
                this.addS3UploadNode(procSpecMap,currentLevelConfig.flattenedParams.get("awsRegion"),puppetDeploymentPath, s3Dir)

                generateFacterPropFile(currentLevelConfig.puppetParams, currentLevelConfig.flattenedParams)
            }

            if(currentLevelConfig.flattenedParams.containsKey("deployment")){
                File deploymentPath = new File(currentLevelConfig.flattenedParams.get("deployment"))
                File deploymentPackage = new File(DEFAULT_BUILD_PATH + "deployment.zip")

                if(deploymentPackage.exists()){
                    deploymentPackage.delete()
                }

                if(deploymentPath.isDirectory()){
                    project.ant.zip(destfile: DEFAULT_BUILD_PATH + "deployment.zip",
                            basedir: deploymentPath.absolutePath)
                }
                else{
                    project.ant.zip(destfile: DEFAULT_BUILD_PATH + "deployment.zip",
                            includes: deploymentPath.absolutePath)
                }
                String s3Dir = dependencyName[dependencyName.indexOf(":") + 1 .. dependencyName.lastIndexOf(":")-1]
                this.addS3UploadNode(procSpecMap,currentLevelConfig.flattenedParams.get("awsRegion"),deploymentPackage.absolutePath, s3Dir)
                currentLevelConfig.flattenedParams.remove("deployment")
            }

            if(!existingStacks.contains(currentLevelConfig.flattenedParams.get("stackName"))){
                existingStacks.add(currentLevelConfig.flattenedParams.get("stackName"))
                this.addCloudformationNode(procSpecMap,currentLevelConfig.flattenedParams,dependencyNameDir)

            }


        }
    }

    private void validateParams(KpConfigDone parentLevelConfig, KpConfigDone currentLevelConfig, String dependencyName){

        //check required params
        for(String requiredName: currentLevelConfig.requiredParams.keySet()){
            if(!parentLevelConfig.dependencies.get(dependencyName).containsKey(requiredName)){
                throw new GradleException("Required parameter was not found: " + requiredName)
            }
        }
        //check if params provided are exposed/valid params
        for(String exposedParam: parentLevelConfig.dependencies.get(dependencyName).keySet()){
            if(!currentLevelConfig.exposedParams.containsKey(exposedParam)){
                throw new GradleException("Parameter isn't exposed/valid: " + exposedParam)
            }
        }
    }

    private void generateFacterPropFile(List<String> puppetParamsList, Map<String, Object> flattenedParams){
        Map<String, Object> puppetParams = extractPuppetParams(puppetParamsList, flattenedParams)

        File propFile = new File(DEFAULT_BUILD_PATH + "kpFacterProps.json")
        String json = JsonOutput.prettyPrint(JsonOutput.toJson(puppetParams))

        //if exists read in and append
        if(propFile.exists()){
            Map<String, Object> contents =  new JsonSlurper().parse(new File(DEFAULT_BUILD_PATH + "kpFacterProps.json"))
            contents << puppetParams
            propFile.write JsonOutput.prettyPrint(JsonOutput.toJson(contents))
        }
        else {
            propFile.write json
        }

    }

    private Map<String, Object> extractPuppetParams(List<String> puppetParams, Map<String, Object> params){
        Map<String, Object> results = new HashMap<>()

        puppetParams.each {puppetParam ->
            if(params.get(puppetParam) == null){
                throw new GradleException("Puppet param isn't defined in the params: " + puppetParam)
            }
            else {
                results.put(puppetParam, params.get(puppetParam))
                params.remove(puppetParam)
            }
        }

        return results
    }

}

package com.opkloud.kloudprints.gradle.tasks

import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import org.gradle.api.DefaultTask
import org.gradle.api.file.ConfigurableFileCollection
import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.TaskAction

class KloudPrintConfigurationTaskDone extends DefaultTask {

    private String DEFAULT_BUILD_PATH = './build/tmp/'
    private String DEFAULT_OUTPUT_PATH = './src/main/infrastructure'
    private String DEFAULT_PROPERTIES_NAME = '/kloud-properties.json'

    private Map<String, Object> configProperties
    private final ConfigurableFileCollection kloudprints


    FileCollection getKloudPrints() {
        return kloudprints
    }

    void setKloudPrints(FileCollection kloudprints) {
        this.kloudprints.setFrom(kloudprints)
    }

    @TaskAction
    void configure() {
        //loop through kloudprints and get dir name
        //merge config maps
        getKloudPrints().getFiles().forEach { file ->
            String fileWithoutExt = file.name.take(file.name.lastIndexOf('.')).take(file.name.lastIndexOf('-'))
            //read file from /build/tmp/<kp_dir>/kloud-properties.json
            //merge with props map
            String fileContents = new File(DEFAULT_BUILD_PATH + fileWithoutExt + DEFAULT_PROPERTIES_NAME).getText('UTF-8')
            JsonSlurper jsonSlurper = new JsonSlurper()
            Map<String, Object> kpProperties = jsonSlurper.parseText(fileContents)
            configProperties << kpProperties
        }
        //print kloud-properties.json to src/main/resources
        File propertyFile = new File(DEFAULT_OUTPUT_PATH + DEFAULT_PROPERTIES_NAME)
        propertyFile.write JsonOutput.prettyPrint(JsonOutput.toJson(configProperties))

    }
}

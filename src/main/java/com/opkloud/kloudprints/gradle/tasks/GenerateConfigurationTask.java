package com.opkloud.kloudprints.gradle.tasks;

import com.opkloud.kloudprints.gradle.extensions.KloudPrintExtension;
import com.opkloud.kloudprints.gradle.utils.KpUtils;
import groovy.json.JsonOutput;
import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.TaskAction;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;

public class GenerateConfigurationTask extends DefaultTask {

    private String DEFAULT_PROPERTIES_NAME = "/kloud-properties.json";

    private KloudPrintExtension extension;

    public void setConfigurations(KloudPrintExtension extension){
        this.extension = extension;
    }

    @TaskAction
    public void generate() throws IOException {
        File kpPropsFile = new File("src/main/resources" + DEFAULT_PROPERTIES_NAME);
        String json = JsonOutput.prettyPrint(JsonOutput.toJson(KpUtils.convertToConfig(extension)));

        if(kpPropsFile.exists()){
            kpPropsFile.delete();
        }

        Files.write(kpPropsFile.toPath(),json.getBytes());

        System.out.println(kpPropsFile.getAbsolutePath());

        File distribution = new File("src/main/infrastructure/distribution");
        if(distribution.exists() && distribution.isDirectory()){
            if(distribution.list().length>0){

                getProject().getAnt().invokeMethod("zip", new HashMap(){{
                    put("destfile", "src/main/resources/infrastructure.zip");
                    put("basedir", distribution.getAbsolutePath());
                }});
            }
        }
    }
}

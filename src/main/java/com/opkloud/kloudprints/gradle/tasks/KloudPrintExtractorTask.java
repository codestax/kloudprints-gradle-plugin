package com.opkloud.kloudprints.gradle.tasks;

import org.gradle.api.DefaultTask;
import org.gradle.api.file.ConfigurableFileCollection;
import org.gradle.api.file.FileCollection;
import org.gradle.api.file.FileTree;
import org.gradle.api.tasks.TaskAction;
import java.io.File;

public class KloudPrintExtractorTask extends DefaultTask {

    ConfigurableFileCollection kloudprints = getProject().files();


//    public KloudPrintExtractorTask() {
//        kloudprints = getProject().files();
//    }

    ConfigurableFileCollection getKloudPrints() {
        return kloudprints;
    }

    public void setKloudPrints(FileCollection kloudprints) {

        this.kloudprints.setFrom(kloudprints);

    }


    @TaskAction
    void extract() {

        System.out.println("\n\n\n Starting KloudPrintExtractorTask.extract():");

        System.out.println("Number of KloudPrint files: " + getKloudPrints().getFiles().size() + "\n\n");

        getKloudPrints().getFiles().forEach( (File file) ->
        {

            FileTree tree = getProject().zipTree(file.getPath());
            String parentOfParent = new File(file.getParent()).getParent();
            String dirName = "";

            System.out.println("File.parent: " + file.getParent());
            System.out.println( "File - Parent of Parent: " + parentOfParent);

            if (file.getParent().contains("files-2.1")) {
                int k = parentOfParent.indexOf("files-2.1");
                dirName = parentOfParent.substring(k + 10);
            }
            else {  /* TODO: Fix this logic... if there isn't a 'files-2.1' in the directory name are we certain there
                             will be a 'repository' in the directory name? */
                int i = file.getParent().indexOf("repository");
                dirName = file.getParent().substring(i + 11);
            }

            System.out.println("Directory Name: " + dirName);

            String formattedDirectoryName = this.formatDirectory(dirName);

            System.out.println("Formatted Directory Name [back in extract()]: " + formattedDirectoryName);


            getProject().copy(copySpec -> {
                copySpec.exclude("META-INF/**");
                copySpec.from(tree);
                copySpec.into("./build/tmp/" + formattedDirectoryName);
            });


            System.out.println("");
            System.out.println( "");

        }); // END for each file loop


    } // END extract() method


    // TODO - This should probably be removed. Let's use normal String replace methods.
    String replace(String str, int index, char replace) {
        char[] chars = str.toCharArray();
        chars[index] = replace;
        return String.valueOf(chars);
    }

    String formatDirectory(String directory) {

        System.out.println( "\n\n\n\n\nIn formatDirectory KloudPrintExtractorTask");
        System.out.println( "directory: " + directory);

        // TODO Refactor this to use simpler string manipulation - get rid of the replace() call
        String versionSep = this.replace(directory, directory.lastIndexOf(File.separator), '-');
        String nameSep = this.replace(versionSep, versionSep.lastIndexOf(File.separator), '-' );

        System.out.println( "versionSep: " + versionSep);
        System.out.println( "nameSep: " + nameSep);


        String formattedDirectoryName = nameSep.replaceAll("[\\\\,\\/]", ".");

        System.out.println( "formattedDirectoryName: " + formattedDirectoryName);
        System.out.println( "\n\n\n\n\n");

        return formattedDirectoryName;  // WAS formattedDirectoryName

    }

}

package com.opkloud.kloudprints.gradle.tasks;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.google.gson.Gson;
import com.opkloud.kloudminions.process.Process;
import com.opkloud.kloudminions.process.ProcessBuilder;
import com.opkloud.kloudprints.gradle.extensions.KloudPrintExtension;
import com.opkloud.kloudprints.gradle.pojos.KpConfig;
import com.opkloud.kloudprints.gradle.pojos.procspec.CloudformationNode;
import com.opkloud.kloudprints.gradle.pojos.procspec.S3Node;
import com.opkloud.kloudprints.gradle.utils.KpUtils;
import groovy.json.JsonOutput;
import groovy.lang.Closure;
import org.apache.commons.lang3.RandomStringUtils;
import org.gradle.api.DefaultTask;
import org.gradle.api.GradleException;
import org.gradle.api.file.ConfigurableFileCollection;
import org.gradle.api.file.FileCollection;
import org.gradle.api.tasks.InputFiles;
import org.gradle.api.tasks.TaskAction;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;

import java.util.*;

public class KloudDeployTask extends DefaultTask {

    private static List<String> ignoredParams = Arrays.asList("awsRegion","stackName");

//    @InputFiles
//    final ConfigurableFileCollection kloudprints = getProject().getObjects().fileCollection();

    private String DEFAULT_BUILD_PATH = System.getProperty("user.dir") + File.separator + "build" + File.separator + "tmp" + File.separator;
    private String DEFAULT_PROPERTIES_NAME = /*File.separator + */"kloud-properties.json";
    private String awsRegion;

    private KloudPrintExtension extension;
    private KpConfig kpConfig;

    private Map<String,String> existingStacks = new HashMap<>();

    private Map<String, Object> procSpecMap = new HashMap() {{
        put("type", "sequence");
        put("nodes", new LinkedList<>());
    }};


    public void setConfigurations(KloudPrintExtension extension) {
        this.extension = extension;
    }

//    @InputFiles
//    public ConfigurableFileCollection getKloudPrints() {
//        return kloudprints;
//    }

//    void setKloudPrints(FileCollection kloudprints) {
//        this.kloudprints = kloudprints;
//    }


    @TaskAction
    public void deploy() throws IOException {

        System.out.println("\n\n\n\n\n\n\n Starting deploy() in KloudDeployTask \n\n\n\n\n\n\n\n\n");
        this.kpConfig = KpUtils.convertToConfig(extension);
        this.awsRegion = this.kpConfig.getAwsRegion();

        if(!this.kpConfig.getDependencies().isEmpty()){
            executeKp(this.kpConfig);
        }

        System.out.println( "\n\n\n\n\n\n\n Starting deploy() in KloudDeployTask \n\n\n\n\n\n\n\n\n");

    }

    void executeKp(KpConfig parentLevelConfig) throws IOException {

        generateProcSpec(parentLevelConfig);

        //add propfile to front of procspec
        this.addS3UploadNode(procSpecMap, awsRegion, DEFAULT_BUILD_PATH + "kpFacterProps.json", null, true);

        if (this.kpConfig.isDeleteStacks() == true) {
            List<Object> reversibleNodes = new LinkedList<>();
            List<Object> procSpecNodes = (LinkedList)procSpecMap.get("nodes");

            for(Object node: procSpecNodes){
                reversibleNodes.add(((LinkedHashMap)node).clone());
            }

            Collections.reverse(reversibleNodes);

            List<Object> nodesToRemove = new ArrayList<>();
            reversibleNodes.forEach( node -> {
                if (((LinkedHashMap)node).get("action")  == "cloud_formation_minion:createStack") {
                    ((LinkedHashMap) node).put ("action","cloud_formation_minion:deleteStack");
                }
                else {
                    nodesToRemove.add(node);
                }
            });
            reversibleNodes.removeAll(nodesToRemove);

            LinkedList<Object> newNodeList = new LinkedList<Object>();
            newNodeList.addAll(reversibleNodes);

            newNodeList.addAll((LinkedList)procSpecMap.get("nodes"));

          procSpecMap.put("nodes",newNodeList);

//            executeProcSpec(procSpecMap);
        } else {
//            executeProcSpec(procSpecMap);
        }
    }

    void generateProcSpec(KpConfig parentLevelConfig) throws IOException {

        for(String dependencyName: parentLevelConfig.getDependencies().keySet()){

            System.out.println("\n\n\n\n\n\n\n In generateProcSpec() in KloudDeployTask");
            System.out.println("dependencyName: " + dependencyName);

            // An exemplar dependencyName ==> tomcat1~com.opkloud.kloudprints:tomcat-kp:1.1.0
            String[] dependencyParts = dependencyName.split("~");

            String dependencyLabel = dependencyParts[0];

            // Example dependencyNameDir ==> com.opkloud.kloudprints-tomcat-kp-1.1.0
            String dependencyNameDir = dependencyParts[1].replaceAll(":","-");


            System.out.println("dependencyParts.size(): " + dependencyParts.length);
            System.out.println("dependencyParts[0]: " + dependencyParts[0]);
            System.out.println("dependencyParts[1]: " + dependencyParts[1]);


            File dependencyNameDirAbsolutePathFile = new File(DEFAULT_BUILD_PATH + dependencyNameDir + File.separator + DEFAULT_PROPERTIES_NAME);


            System.out.println("dependencyNameDirAbsolutePathFile: " + dependencyNameDirAbsolutePathFile);
            System.out.println("\n\n\n\n\n\n\n\n");

            KpConfig currentLevelConfig = new Gson().fromJson(new FileReader(dependencyNameDirAbsolutePathFile), KpConfig.class);

            parentLevelConfig.getDependencies().get(dependencyName).forEach(currentLevelConfig.getFlattenedParams()::put);

            if(!currentLevelConfig.getDependencies().isEmpty()) {
                this.generateProcSpec(currentLevelConfig);
            }

            validateParams(parentLevelConfig, currentLevelConfig, dependencyName);

            String infrastructureDeploymentPath = DEFAULT_BUILD_PATH + dependencyNameDir + File.separator + "infrastructure.zip";

            File infrastructureDir = new File(infrastructureDeploymentPath);

            String s3Key = dependencyLabel+"-"+getHex();

            if(currentLevelConfig.getFlattenedParams().containsKey("deployment")){
                File deploymentPath = new File(currentLevelConfig.getFlattenedParams().get("deployment").toString());
                File deploymentPackage = new File(DEFAULT_BUILD_PATH + dependencyNameDir + File.separator + s3Key + File.separator + "deployment.zip");

                if(deploymentPackage.exists()){
                    deploymentPackage.delete();
                }

                if(deploymentPath.isDirectory()){

                    getProject().getAnt().invokeMethod("zip", new HashMap(){{
                        put("destfile", deploymentPackage.getAbsolutePath());
                        put("basedir", deploymentPath.getAbsolutePath());
                    }});
                }
                else {
                    currentLevelConfig.getPuppetParams().add("deploymentfilename");
                    currentLevelConfig.getFlattenedParams().put("deploymentfilename", deploymentPath.getName());

                    // TODO: Refactor this code. Kept the same to keep transitioning from groovy to java simple
                    getProject().getAnt().invokeMethod("zip", new Object[] {new HashMap(){{
                        put("destfile", deploymentPackage.getAbsolutePath());

                    }}, new Closure<Object>(null) {
                        @Override
                        public Object call(Object... args) {
                            getAnt().invokeMethod("fileset", new Object[] {
                                    new HashMap<String, String>() {
                                        {
                                            this.put("file", deploymentPath.getAbsolutePath());
                                        }
                                    }});
                            return null;
                        }
                    }});
                }

                this.addS3UploadNode(procSpecMap,awsRegion,deploymentPackage.getAbsolutePath(), s3Key);
                currentLevelConfig.getFlattenedParams().remove("deployment");
            }

            if(infrastructureDir.exists() && infrastructureDir.isFile()){

                currentLevelConfig.getFlattenedParams().put("DeploymentBucket", this.kpConfig.getDeploymentBucket());
                currentLevelConfig.getFlattenedParams().put("BucketKey", s3Key);

                this.addS3UploadNode(procSpecMap,awsRegion,infrastructureDeploymentPath, s3Key);
            }

            generateFacterPropFile(currentLevelConfig.getPuppetParams(), currentLevelConfig.getFlattenedParams());

            if(currentLevelConfig.isSingleton()){

                if((existingStacks.containsKey(dependencyNameDir) && existingStacks.get(dependencyNameDir) != awsRegion ||
                        (!existingStacks.containsKey(dependencyNameDir)))){
                    existingStacks.put(dependencyNameDir,awsRegion);
                    this.addCloudformationNode(procSpecMap,currentLevelConfig.getFlattenedParams(),dependencyNameDir);
                }
            }
            else {
                existingStacks.put(dependencyNameDir,awsRegion);
                this.addCloudformationNode(procSpecMap,currentLevelConfig.getFlattenedParams(),dependencyNameDir);
            }
        }
    }


    private void validateParams(KpConfig parentLevelConfig, KpConfig currentLevelConfig, String dependencyName){

        //check required params
        for(String requiredName: currentLevelConfig.getRequiredParams().keySet()){
            if(!parentLevelConfig.getDependencies().get(dependencyName).containsKey(requiredName)){
                throw new GradleException("Required parameter was not found: " + requiredName);
            }
        }
        //check if params provided are exposed/valid params
        for(String exposedParam: parentLevelConfig.getDependencies().get(dependencyName).keySet()){
            if(!currentLevelConfig.getExposedParams().containsKey(exposedParam)){
                throw new GradleException("Parameter isn't exposed/valid: " + exposedParam);
            }
        }
    }

    private Map<String, Object> filterConfig(Map<String, Object> config){
        ignoredParams.forEach(param ->{
                config.remove(param);
        });

        return config;
    }

    private void executeProcSpec(Map<String, Object> procSpecMap){
        ProcessBuilder processBuilder = new ProcessBuilder();

        processBuilder.setProcessSpec(procSpecMap);
        processBuilder.setGlobalContextMap(new HashMap<String, Object>());


        AWSCredentials defaultCredentials = new DefaultAWSCredentialsProviderChain().getCredentials();
        processBuilder.setAWSCredentials(defaultCredentials);

        try{
            Process process = processBuilder.build();
            process.execute();
        }
        catch (Exception e){
            throw new GradleException("There was an exception executing a kloudprint: ",e);
        }

    }


    private void addCloudformationNode(Map procSpecMap, Map<String, Object> flattenedConfig, String dependencyName ){

        LinkedList<Object> procSpecNodes = (LinkedList)procSpecMap.get("nodes");
        CloudformationNode cloudformationNode = new CloudformationNode();

        cloudformationNode.setAws_region(awsRegion);
        cloudformationNode.setAws_stack_name(flattenedConfig.get("stackName").toString());
        cloudformationNode.setDeploymentBucket(this.kpConfig.getDeploymentBucket());
        cloudformationNode.setCf_script_filename(DEFAULT_BUILD_PATH + dependencyName + File.separator + "cloudformations" + File.separator + "default.cf");
        cloudformationNode.setCf_parameters(filterConfig(flattenedConfig));

        procSpecNodes.add(KpUtils.convertObjToMap(cloudformationNode));
    }

    private void addS3UploadNode(Map procSpecMap, String awsRegion, String fileToUpload, String s3Dir, boolean addToFront){

        LinkedList<Object> procSpecNodes = (LinkedList)procSpecMap.get("nodes");
        S3Node s3Node = new S3Node();

        s3Node.setAws_region(awsRegion);
        s3Node.setFiles(Arrays.asList(fileToUpload));
        s3Node.setS3_dir(s3Dir);
        s3Node.setBucket(this.kpConfig.getDeploymentBucket());

        if(addToFront){
            procSpecNodes.addFirst(KpUtils.convertObjToMap(s3Node));
        }
        else {
            procSpecNodes.add(KpUtils.convertObjToMap(s3Node));
        }
    }

    private void addS3UploadNode(Map procSpecMap, String awsRegion, String fileToUpload, String s3Dir) {
        this.addS3UploadNode(procSpecMap,awsRegion,fileToUpload,s3Dir,false);
    }


        private void generateFacterPropFile(List<String> puppetParamsList, Map<String, Object> flattenedParams) throws IOException {
        Map<String, Object> puppetParams = extractPuppetParams(puppetParamsList, flattenedParams);

//        Path propFilePath = Paths.get(DEFAULT_BUILD_PATH + "kpFacterProps.json");
        File propFile = new File(DEFAULT_BUILD_PATH + "kpFacterProps.json");
        String json = JsonOutput.prettyPrint(JsonOutput.toJson(puppetParams));

        //if exists read in and append
        if(propFile.exists()){
//            Map<String, Object> contents =  new JsonSlurper().parse(new File(DEFAULT_BUILD_PATH + "kpFacterProps.json"));
            Map<String, Object> contents =  new Gson().fromJson(new FileReader(DEFAULT_BUILD_PATH + "kpFacterProps.json"), Map.class);
            puppetParams.forEach(contents::put);
            String contentsJson = JsonOutput.prettyPrint(JsonOutput.toJson(puppetParams));

            Files.write(propFile.toPath(),contentsJson.getBytes());
        }
        else {
            Files.write(propFile.toPath(),json.getBytes());
        }

    }

    private Map<String, Object> extractPuppetParams(List<String> puppetParams, Map<String, Object> params){
        Map<String, Object> results = new HashMap<>();

        puppetParams.forEach(puppetParam -> {
            if(params.get("puppetParam") == null){
                throw new GradleException("Puppet param isn't defined in the params: " + puppetParam);
            }
            else {
                results.put("puppetParam", params.get("puppetParam"));
                params.remove("puppetParam");
            }
        });

        return results;
    }

    /* TODO: Let's look at this as a way to create random S3 Key Names... maybe a UUID might work better. */
    private String getHex(){
        return RandomStringUtils.randomNumeric(4);
    }

}

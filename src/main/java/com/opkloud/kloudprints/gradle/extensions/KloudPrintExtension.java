package com.opkloud.kloudprints.gradle.extensions;

import org.gradle.api.GradleException;

import javax.annotation.Nullable;
import java.util.*;

public class KloudPrintExtension {

    String deploymentBucket;
    String awsRegion;
    boolean deleteStacks;
    boolean singleton;

    List<String> order = new ArrayList<>();
    List<String> puppetParams = new ArrayList<>();
    List<String> labels = new ArrayList<>();

    Map<String, Map<String, Object>> dependencies = new LinkedHashMap<>();

    Map<String, Map<String, Object>> params = new HashMap<>();
    Map<String, Object> flattenedParams = new HashMap<>();
    Map<String, Map<String, Object>> exposedParams = new HashMap<>();
    Map<String, Map<String, Object>> requiredParams = new HashMap<>();

    void awsRegion(String awsRegion){
        this.awsRegion = awsRegion;
    }

    void deploymentBucket(String deploymentBucket){
        this.deploymentBucket = deploymentBucket;
    }

    void deleteStacks(boolean deleteStacks){
        this.deleteStacks = deleteStacks;
    }

    void singleton(boolean singleton){
        this.singleton = singleton;
    }

    void order(String... kloudprint) {
        order.addAll(Arrays.asList(kloudprint));
    }

    void puppetParams(String... params){
        puppetParams.addAll(Arrays.asList(params));
    }

    void param(String paramNameOrShorthand, @Nullable String paramDefaultValue, boolean required, boolean exposed ) {
        if (!paramNameOrShorthand.contains(":")) {
            Map<String, Object> paramConfig = new HashMap<>();

            paramConfig.put("name",paramNameOrShorthand);
            paramConfig.put("value",paramDefaultValue);
            paramConfig.put("required",required);
            paramConfig.put("exposed",exposed);

            this.param(paramConfig);
        }
        else {
            this.processShorthandParam(paramNameOrShorthand);
        }

    }

    void param(String paramNameOrShorthand){
        param(paramNameOrShorthand,null,false,true);
    }

    void param(String paramNameOrShorthand, String defaultValue){
        param(paramNameOrShorthand,defaultValue,false,true);
    }


    void processShorthandParam(String paramString) {
        String[] paramProps = paramString.split(":");
        Map<String, Object> paramConfig = new HashMap<>();

        if (paramProps.length == 2) {
            paramConfig.put("name",paramProps[0]);
            paramConfig.put("value",paramProps[1]);
        }
        else if (paramProps.length == 3) {
            paramConfig.put("name",paramProps[0]);
            paramConfig.put("value",paramProps[1]);

            if (paramProps[2].equalsIgnoreCase("required")) {
                paramConfig.put("required",true);
            } else {
                paramConfig.put("exposed",false);
            }
        }
        else if (paramProps.length == 4) {
            paramConfig.put("name",paramProps[0]);
            paramConfig.put("value",paramProps[1]);
            paramConfig.put("required",true);
            paramConfig.put("exposed",false);
        }

        this.param(paramConfig);
    }


    void param(Map<String, Object> paramConfig) {

        validateParams(paramConfig);

        this.params.put(paramConfig.get("name").toString(), paramConfig);
        this.flattenedParams.put(paramConfig.get("name").toString(), paramConfig.get("value"));

        if ((boolean)paramConfig.get("exposed") == true) {
            this.exposedParams.put(paramConfig.get("name").toString(), paramConfig);
        }

        if ((boolean)paramConfig.get("required") == true) {
            this.requiredParams.put(paramConfig.get("name").toString(), paramConfig);
        }

    }

    private void validateParams(Map<String, Object> paramConfig) {
        if (paramConfig == null) {
            // Throw Exception
        }

        // Ensure there is a paramConfig.name
        if (!paramConfig.containsKey("name")) {
            // Throw Exception
        }

        if (paramConfig.get("name") != null && paramConfig.get("value") == null) {
            paramConfig.put("required",true);
            paramConfig.put("exposed",true);
        }

        if (paramConfig.get("name") != null && paramConfig.get("value") != null && paramConfig.get("required") == null && paramConfig.get("exposed") == null) {
            paramConfig.put("required",false);
            paramConfig.put("exposed",true);
        }

        if (paramConfig.get("name") != null && paramConfig.get("value") != null && paramConfig.get("required") != null && paramConfig.get("exposed") == null) {
            paramConfig.put("exposed",true);
        }

        if (paramConfig.get("name") != null && paramConfig.get("value") != null && paramConfig.get("required") == null && paramConfig.get("exposed") != null) {
            paramConfig.put("required",false);
        }

        if (((boolean)paramConfig.get("exposed") == false && (boolean)paramConfig.get("required") == false) || ((boolean)paramConfig.get("required") == true || (boolean)paramConfig.get("exposed") == false)) {
            //throw error
        }
    }

    void dependency(String label, String artifactID, Map<String, Object> artifactConfig) {

        if(labels.contains(label)){
            throw new GradleException("Dependency labels must be unique. " + label + " exists already.");
        }

        labels.add(label);
        this.dependencies.put(label + "~" + artifactID, artifactConfig);
    }

    public String getDeploymentBucket() {
        return deploymentBucket;
    }

    public void setDeploymentBucket(String deploymentBucket) {
        this.deploymentBucket = deploymentBucket;
    }

    public String getAwsRegion() {
        return awsRegion;
    }

    public void setAwsRegion(String awsRegion) {
        this.awsRegion = awsRegion;
    }

    public boolean isDeleteStacks() {
        return deleteStacks;
    }

    public void setDeleteStacks(boolean deleteStacks) {
        this.deleteStacks = deleteStacks;
    }

    public boolean isSingleton() {
        return singleton;
    }

    public void setSingleton(boolean singleton) {
        this.singleton = singleton;
    }

    public List<String> getOrder() {
        return order;
    }

    public void setOrder(List<String> order) {
        this.order = order;
    }

    public List<String> getPuppetParams() {
        return puppetParams;
    }

    public void setPuppetParams(List<String> puppetParams) {
        this.puppetParams = puppetParams;
    }

    public List<String> getLabels() {
        return labels;
    }

    public void setLabels(List<String> labels) {
        this.labels = labels;
    }

    public Map<String, Map<String, Object>> getDependencies() {
        return dependencies;
    }

    public void setDependencies(Map<String, Map<String, Object>> dependencies) {
        this.dependencies = dependencies;
    }

    public Map<String, Map<String, Object>> getParams() {
        return params;
    }

    public void setParams(Map<String, Map<String, Object>> params) {
        this.params = params;
    }

    public Map<String, Object> getFlattenedParams() {
        return flattenedParams;
    }

    public void setFlattenedParams(Map<String, Object> flattenedParams) {
        this.flattenedParams = flattenedParams;
    }

    public Map<String, Map<String, Object>> getExposedParams() {
        return exposedParams;
    }

    public void setExposedParams(Map<String, Map<String, Object>> exposedParams) {
        this.exposedParams = exposedParams;
    }

    public Map<String, Map<String, Object>> getRequiredParams() {
        return requiredParams;
    }

    public void setRequiredParams(Map<String, Map<String, Object>> requiredParams) {
        this.requiredParams = requiredParams;
    }


}

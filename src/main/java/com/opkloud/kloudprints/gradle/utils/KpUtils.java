package com.opkloud.kloudprints.gradle.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.opkloud.kloudprints.gradle.extensions.KloudPrintExtension;
import com.opkloud.kloudprints.gradle.pojos.KpConfig;

import java.util.Map;

public class KpUtils {

    public static KpConfig convertToConfig(KloudPrintExtension extension){
        KpConfig config = new KpConfig();

        config.setParams(extension.getParams());
        config.setRequiredParams(extension.getRequiredParams());
        config.setDependencies(extension.getDependencies());
        config.setExposedParams(extension.getExposedParams());
        config.setDeploymentBucket(extension.getDeploymentBucket());
        config.setPuppetParams(extension.getPuppetParams());
        config.setDeleteStacks(extension.isDeleteStacks());
        config.setSingleton(extension.isSingleton());
        config.setFlattenedParams(extension.getFlattenedParams());
        config.setAwsRegion(extension.getAwsRegion());

        return config;
    }

    public static Map<String, Object> convertObjToMap(Object object){

        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> map = objectMapper.convertValue(object, Map.class);
        return map;
    }
}

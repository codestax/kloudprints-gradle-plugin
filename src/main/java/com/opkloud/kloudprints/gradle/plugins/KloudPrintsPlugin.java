package com.opkloud.kloudprints.gradle.plugins;

import com.opkloud.kloudprints.gradle.extensions.KloudPrintExtension;
import com.opkloud.kloudprints.gradle.tasks.GenerateConfigurationTask;
import com.opkloud.kloudprints.gradle.tasks.KloudDeployTask;
import com.opkloud.kloudprints.gradle.tasks.KloudPrintExtractorTask;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.plugins.JavaPluginConvention;
import org.gradle.api.tasks.SourceSet;
import org.gradle.api.tasks.bundling.Jar;


import java.util.Arrays;
import java.util.HashMap;

public class KloudPrintsPlugin implements Plugin<Project> {

    private static final String TASK_GROUP_NAME = "KloudPrints";

    private static final String DEPENDENCY_CONFIGURATION_NAME = "kloudprint";
    private static final String KLOUD_EXTRACT_TASK_NAME = "kpExtract";
    private static final String KLOUD_DEPLOY_TASK_NAME = "kpDeploy";
    private static final String KLOUD_PACKAGE_TASK_NAME = "kpPackage";
    private static final String GENERATE_CONFIG = "generateKpConfig";

    private static final String DEPENDENCY_CONFIGURATION_DESCRIPTION = "The KloudPrints to retrieve for this project.";
    private static final String KLOUD_EXTRACT_TASK_DESCRIPTION = "Extracts content of specified KloudPrints to directory.";
    private static final String KLOUD_DEPLOY_TASK_DESCRIPTION = "Deploys infrastructure defined by the process-spec and KloudPrint defined.";
    private static final String KLOUD_PACKAGE_TASK_DESCRIPTION = "Packages a KloudPrint project into a jar file";
    private static final String GENERATE_CONFIG_TASK_DESCRIPTION = "Generates kloudprints-props for the kloudprints";

    private static final String DEFAULT_INFRA_DIR = "src/main/infrastructure";
    private static final String DEFAULT_RESOURCES_DIR = "src/main/resources";

    public void apply(Project project) {

        final Configuration config = project.getConfigurations().create(DEPENDENCY_CONFIGURATION_NAME)
                .setVisible(true)
                .setTransitive(true)
                .setDescription(DEPENDENCY_CONFIGURATION_DESCRIPTION);
//                config.extendsFrom(project.getConfigurations().getByName("implementation"));
//
//        config.setCanBeResolved(true);
//        config.setCanBeConsumed(true);

        project.getConfigurations().getByName("implementation").extendsFrom(config);

        KloudPrintExtension kloudPrintExtension = project.getExtensions().create("kloudprints", KloudPrintExtension.class);

        configureKloudExtractorTask(project, config);
        configureKloudDeployTask(project, kloudPrintExtension, config);
        configureKloudPackageTask(project, configureSourceSets(project));
        configureGenerateKPConfig(project, kloudPrintExtension);
    }

    void configureKloudExtractorTask(Project project, Configuration config) {
        Task kmTask = project.getTasks().create(KLOUD_EXTRACT_TASK_NAME, KloudPrintExtractorTask.class,  task ->{
                task.setKloudPrints(config);
        });

        kmTask.setGroup(TASK_GROUP_NAME);
        kmTask.setDescription(KLOUD_EXTRACT_TASK_DESCRIPTION);
    }

    void configureKloudDeployTask(Project project, KloudPrintExtension kloudPrintExtension, Configuration config) {

        Task kmTask = project.getTasks().create(KLOUD_DEPLOY_TASK_NAME, KloudDeployTask.class, kloudDeployTaskDone -> {
            kloudDeployTaskDone.setConfigurations(kloudPrintExtension);
        });

        kmTask.dependsOn("build", KLOUD_EXTRACT_TASK_NAME);
        kmTask.setGroup(TASK_GROUP_NAME);
        kmTask.setDescription(KLOUD_DEPLOY_TASK_DESCRIPTION);
    }

    void configureKloudPackageTask(Project project, SourceSet main) {
        Task kmTask = project.getTasks().create(KLOUD_PACKAGE_TASK_NAME, Jar.class, jar ->{
                jar.setProperty("includeEmptyDirs",true);
//                jar.setProperty("includes",main.getResources());
                jar.from(main.getResources());
        });
        kmTask.dependsOn(GENERATE_CONFIG);
        kmTask.setGroup(TASK_GROUP_NAME);
        kmTask.setDescription(KLOUD_PACKAGE_TASK_DESCRIPTION);
    }

    void configureGenerateKPConfig(Project project, KloudPrintExtension kloudPrintExtension) {
        Task kmTask = project.getTasks().create(GENERATE_CONFIG, GenerateConfigurationTask.class, task ->{
                task.setConfigurations(kloudPrintExtension);
        });
        kmTask.setGroup(TASK_GROUP_NAME);
        kmTask.setDescription(GENERATE_CONFIG_TASK_DESCRIPTION);
    }

    SourceSet configureSourceSets(Project project){
        JavaPluginConvention javaPluginConvention = project.getConvention().getPlugin(JavaPluginConvention.class);
        SourceSet main = javaPluginConvention.getSourceSets().getByName(SourceSet.MAIN_SOURCE_SET_NAME);
        main.getResources().setSrcDirs(Arrays.asList(DEFAULT_INFRA_DIR, DEFAULT_RESOURCES_DIR));

        return main;
    }

}

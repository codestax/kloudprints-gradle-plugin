package com.opkloud.kloudprints.gradle.pojos.procspec;

import java.util.HashMap;
import java.util.Map;

public class CloudformationNode {
    String type = "action";
    String id = "cloudformation";
    String action = "cloud_formation_minion:createStack";
    String aws_region;
    String aws_stack_name;
    String DeploymentBucket;
    String cf_script_filename;
    Map<String,Object> cf_parameters = new HashMap<>();

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getAws_region() {
        return aws_region;
    }

    public void setAws_region(String aws_region) {
        this.aws_region = aws_region;
    }

    public String getAws_stack_name() {
        return aws_stack_name;
    }

    public void setAws_stack_name(String aws_stack_name) {
        this.aws_stack_name = aws_stack_name;
    }

    public String getDeploymentBucket() {
        return DeploymentBucket;
    }

    public void setDeploymentBucket(String deploymentBucket) {
        DeploymentBucket = deploymentBucket;
    }

    public String getCf_script_filename() {
        return cf_script_filename;
    }

    public void setCf_script_filename(String cf_script_filename) {
        this.cf_script_filename = cf_script_filename;
    }

    public Map<String, Object> getCf_parameters() {
        return cf_parameters;
    }

    public void setCf_parameters(Map<String, Object> cf_parameters) {
        this.cf_parameters = cf_parameters;
    }
}

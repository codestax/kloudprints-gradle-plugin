package com.opkloud.kloudprints.gradle.pojos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KpConfig {
    String deploymentBucket;
    String awsRegion;

    boolean deleteStacks = false;
    boolean singleton = false;

    List<String> puppetParams = new ArrayList<>();

    Map<String,Object> flattenedParams = new HashMap<>();

    Map<String, Map<String, Object>> dependencies = new HashMap<>();
    Map<String, Map<String, Object>> params = new HashMap<>();
    Map<String, Map<String, Object>> exposedParams = new HashMap<>();
    Map<String, Map<String, Object>> requiredParams = new HashMap<>();

    public String getDeploymentBucket() {
        return deploymentBucket;
    }

    public void setDeploymentBucket(String deploymentBucket) {
        this.deploymentBucket = deploymentBucket;
    }

    public String getAwsRegion() {
        return awsRegion;
    }

    public void setAwsRegion(String awsRegion) {
        this.awsRegion = awsRegion;
    }

    public boolean isDeleteStacks() {
        return deleteStacks;
    }

    public void setDeleteStacks(boolean deleteStacks) {
        this.deleteStacks = deleteStacks;
    }

    public boolean isSingleton() {
        return singleton;
    }

    public void setSingleton(boolean singleton) {
        this.singleton = singleton;
    }

    public List<String> getPuppetParams() {
        return puppetParams;
    }

    public void setPuppetParams(List<String> puppetParams) {
        this.puppetParams = puppetParams;
    }

    public Map<String, Object> getFlattenedParams() {
        return flattenedParams;
    }

    public void setFlattenedParams(Map<String, Object> flattenedParams) {
        this.flattenedParams = flattenedParams;
    }

    public Map<String, Map<String, Object>> getDependencies() {
        return dependencies;
    }

    public void setDependencies(Map<String, Map<String, Object>> dependencies) {
        this.dependencies = dependencies;
    }

    public Map<String, Map<String, Object>> getParams() {
        return params;
    }

    public void setParams(Map<String, Map<String, Object>> params) {
        this.params = params;
    }

    public Map<String, Map<String, Object>> getExposedParams() {
        return exposedParams;
    }

    public void setExposedParams(Map<String, Map<String, Object>> exposedParams) {
        this.exposedParams = exposedParams;
    }

    public Map<String, Map<String, Object>> getRequiredParams() {
        return requiredParams;
    }

    public void setRequiredParams(Map<String, Map<String, Object>> requiredParams) {
        this.requiredParams = requiredParams;
    }
}

package com.opkloud.kloudprints.gradle.pojos.procspec;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class S3Node {
    String type = "action";
    String id = "s3Upload";
    String action = "s3_minion:uploadFiles";
    String aws_region;
    List<String> files = new ArrayList<>();
    String s3_dir;
    String bucket;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getAws_region() {
        return aws_region;
    }

    public void setAws_region(String aws_region) {
        this.aws_region = aws_region;
    }

    public List<String> getFiles() {
        return files;
    }

    public void setFiles(List<String> files) {
        this.files = files;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getS3_dir() {
        return s3_dir;
    }

    public void setS3_dir(String s3_dir) {
        this.s3_dir = s3_dir;
    }
}
